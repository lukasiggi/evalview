/* Sources:
    - basics: https://www.html5rocks.com/en/tutorials/file/dndfiles/
    - extended: https://stackoverflow.com/questions/18480550/how-to-load-all-the-images-from-one-of-my-folder-into-my-web-page-using-jquery/18480589#18480589
 */

let DEBUG = true;

//let imgArray = ["album_1.jpg", "silencer-goodie-18.jpg"];
let imgArray = [["1.jpg", "2.jpg", "3.jpg"],
                ["4.jpg", "5.jpg", "6.jpg"]];
/* Project root folder is needed as well! */
let basePath = "/evalview/";
let path = ""; /* Will be initialized on window.onload */
let dirIdx = 0;
let imgIdx = 0;

let curImg = document.getElementById("eval-img-1");
let dirSelector = document.getElementById("fig-dir");

/* Load default image when site is loaded */
window.onload = changeDir;

/* Change directory if dropdown-selected */
dirSelector.onchange = function() {changeDir()};

/* Cycle images when selected by buttons */
document.getElementById("cycle-imgs-left").onclick = function() {cycleImg('l')};
document.getElementById("cycle-imgs-right").onclick = function() {cycleImg('r')};

let ddMenu = document.getElementById("dropbtn-1");
let opt = document.getElementById("fig1").addEventListener("click", function(e) {
    console.log("Clicked!");
    ddMenu.innerText = document.getElementById("fig1").innerText;
})

/* Cycle images when selected by left-right keys */
document.addEventListener("keyup", function(e) {
    if (e.key == 'ArrowLeft') {
        cycleImg('l')
    } else if (e.key == 'ArrowRight') {
        cycleImg('r');
    }
})

function changeDir() {
    dirIdx = dirSelector.selectedIndex;
    path = basePath + dirSelector.options[dirIdx].value + '/';
    console.log("Selected path: " + path);
    curImg.setAttribute("src", path + imgArray[dirIdx][imgIdx]);
}

function cycleImg(direction) {
    /* set new image index */
    if (direction == 'r') {
        if (imgIdx < (imgArray[dirIdx].length - 1)) {
            imgIdx += 1;
        } else {
            imgIdx = 0;
        }
    } else if (direction == 'l') {
        if (imgIdx > 0) {
            imgIdx -= 1;
        } else {
            imgIdx = imgArray[dirIdx].length - 1;
        }
    } else {
        alert('Impossible direction in cycleImg.');
    }

    if (DEBUG) { console.log("imgIdx: " + imgIdx); }

    path = path || "/figures/";
    let newImg = imgArray[dirIdx][imgIdx];
    //let newImgStr = '<img src="' + path + newImg + '">';
    let newImgStr = path + newImg;
    //let curImg = document.getElementById("eval-img-1");
    if (curImg) {
        curImg.setAttribute("src", newImgStr);
        if (DEBUG) { console.log("img src to set: " + newImgStr); }
    }
    else {
        console.log("Could not retrieve image.");
    }
}

/* DISPLAY DIRECTORY STRUCTURE */

// document.getElementById("change-img").onclick = function (event) {
//     path = path || "/figures/";
//     let newImg = img_array[1];
//     //let newImgStr = '<img src="' + path + newImg + '">';
//     let newImgStr = path + newImg;
//     let curImg = document.getElementById("eval-img-1")
//     if (curImg) {
//         console.log("img src to set: " + newImgStr);
//         curImg.setAttribute("src", newImgStr);
//     }
//     else {
//         console.log("Could not retrieve image.");
//     }
// }

// document.getElementById("change-img").onclick = function (event) {
//     path = path || "/figures/";
//     let newImg = img_array[1];
//     //let newImgStr = '<img src="' + path + newImg + '">';
//     let newImgStr = path + newImg;
//     let curImg = document.getElementById("eval-img-1")
//     if (curImg) {
//         console.log("img src to set: " + newImgStr);
//         curImg.setAttribute("src", newImgStr);
//     }
//     else {
//         console.log("Could not retrieve image.");
//     }
// }
//
// /* Use jQuery and ajax */
//
// var folder = "";
//
// $.ajax({
//     url : folder,
//     success: function (data) {
//         $(data).find("a").attr("href", function (i, val) {
//             if( val.match(/\.(jpe?g|png|gif)$/) ) {
//                 $("body").append( "<img src='"+ folder + val +"'>" );
//             }
//         });
//     }
// });

//
// let reader = new FileReader();
//
// /* Check if FileAPI is supported */
// if (window.File && window.FileReader && window.FileList && window.Blob) {
//     handleImageView();
// } else {
//     alert("FileAPI not supported!");
// }
//
//
// document.getElementById("file-input").onchange = function (event) {
//     let tgt = event.target || window.event.srcElement,
//         files = tgt.files;
//     //reader = new FileReader();
//     //reader.onerror = errorHandler;
//     //reader.onprogress = updateProgress;
//
//     //reader.onload = () => showImage(reader);
//
//     let downloadingImage = new Image();
//
//     reader.onload = function() {
//     }
//
//     console.log("handleImageView() called");
//
//     loadedFile = document.getElementById("file-input");
//
//     console.log(imgBox.item(0).innerHTML);
//
//     //let img = new Image();
//     //img.src = "figures/"
//
//     //document.addEventListener("")
//     //reader.
//
// }
//
// function showImage(freader) {
//     let imgBox = document.getElementById("eval-img-1");
//     if (imgBox) {
//         imgBox.onload = () => getImageData(imgBox);
//     }
// }
//
// function getImageData(img) {
//
// }
//
// function errorHandler() {
//     reader.abort();
// }
//
// function updateProgress() {
//
// }
//
// function preloader()
//
// {
//
//     // counter
//     var i = 0;
//
//     // create object
//     imageObj = new Image();
//
//     // set image list
//     images = new Array();
//     images[0]="image1.jpg"
//     images[1]="image2.jpg"
//     images[2]="image3.jpg"
//     images[3]="image4.jpg"
//
//     // start preloading
//     for(i=0; i<=3; i++)
//     {
//         imageObj.src=images[i];
//     }
//
//
// }